/*
 * Implementation of functions that find values in strings.
 *****
 * YOU MAY NOT USE ANY FUNCTIONS FROM <string.h>
 *****
 */

#include <stdlib.h>
#include <stdbool.h>

#include "find.h"

#define NOT_FOUND (-1)	// integer indicator for not found.

/*
 * Return the index of the first occurrence of <ch> in <string>,
 * or (-1) if the <ch> is not in <string>.
 */
int find_ch_index(char string[], char ch) {
	// return NOT_FOUND ;	// placeholder

	// go through string, if the index exists, keep going
	// if string[index] == ch, loop will stop and return

	int index = 0 ;
	while(string[index] && string[index] != ch) {
		index++;
	}
	
	// return not found if index out of bounds

	if (string[index])
		return index;
	else
		return NOT_FOUND;
}

/*
 * Return a pointer to the first occurrence of <ch> in <string>,
 * or NULL if the <ch> is not in <string>.
 *****
 * YOU MAY *NOT* USE INTEGERS OR ARRAY INDEXING.
 *****
 */
char *find_ch_ptr(char *string, char ch) {
	// return NULL ;	// placeholder

	// same thing as before but increment pointers
	// string is equal to pointer that is s>t>r>i>n>g so ptr++

	char *p_str = string;

	while(*p_str && *p_str != ch)
		p_str++;

	if (*p_str)
		return p_str;
	else
		return NULL;
}

/*
 * Return the index of the first occurrence of any character in <stop>
 * in the given <string>, or (-1) if the <string> contains no character
 * in <stop>.
 */
int find_any_index(char string[], char stop[]) {
	// return NOT_FOUND ;	// placeholder

	// go through first string
	// go through stop string within first string
	// same as first for both of the loops

	int index = 0;
	int stop_index;

	while(string[index]) {
		stop_index = 0;
		while(stop[stop_index] && stop[stop_index] != string[index])
			stop_index++;

		if(stop[stop_index]) 
			return index; // breaks loop
		
		index++;
	}

	return NOT_FOUND;
}

/*
 * Return a pointer to the first occurrence of any character in <stop>
 * in the given <string> or NULL if the <string> contains no characters
 * in <stop>.
 *****
 * YOU MAY *NOT* USE INTEGERS OR ARRAY INDEXING.
 *****
 */
char *find_any_ptr(char *string, char* stop) {
	// return NULL ;	// placeholder

    char *p_str = string;
    char *p_stop = stop;

    while (*p_str) {
        p_stop = stop;

        while (*p_stop != *p_str && *p_stop)
            p_stop++;

        if (*p_stop)
            return p_str;
        
        p_str++;
    }
    
    return NULL;
}

/*
 * Return a pointer to the first character of the first occurrence of
 * <substr> in the given <string> or NULL if <substr> is not a substring
 * of <string>.
 * Note: An empty <substr> ("") matches *any* <string> at the <string>'s
 * start.
 *****
 * YOU MAY *NOT* USE INTEGERS OR ARRAY INDEXING.
 *****
 */
char *find_substr(char *string, char* substr) {
	// return NULL ;	// placeholder
    char *p_final_str = string;

    char *p_substr = substr;
    char *p_str = string;

    while (true) {
        p_substr = substr;
        p_str = p_final_str;

        while (*p_substr == *p_str && *p_substr) {
            p_str++;
            p_str++;
        }

        if (!*p_substr)
            return p_final_str;

        if (!*p_str)
            return NULL;

        p_final_str++;
    }
}
