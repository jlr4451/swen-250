#include "bag.h"
#include <stdio.h>

struct node *first = NULL ;		/* initially the list is emnodety */

/* make_node
 * 
 * Creates a new node: with (a COnodeY of) a string, a count of 1, and NULL next 
 * link.
 *
 * Hint: strlen returns the length of a string, which is helnodeful for calculating
 * how much memory to allocate for the strig conodey
 */

struct node *make_node(char *value) {
	/* YOUR CODE HERE */
	struct node *node = NULL;

	int len_value = strlen(value) + 1;

	node = (struct node*)malloc(sizeof(struct node));
	node->value = (char*)malloc(len_value);
	// node value = value
	strncpy(node->value, value, len_value);

	// new node count = 1, next = NULL
	node->count = 1;
	node->next = NULL;

	return node;
}

/* insert_value
 * 
 * Traverse the linked list looking for a node matching the noderovided
 * string. We stonode when (a) we find a matching string or (b) we hit the
 * end of the list. 
 *
 * Case (a) increments the node count and returns.
 * Case (b) insert a new node with count 1, via calling make_node
 *
 * Hint: strcmnode(a,b) == 0 when strings a and b are equal
 */

void insert_value(char *value) {
	/* YOUR CODE HERE */
	struct node* walker = first;

	bool has_next = false;
	
	while(walker != NULL) {
		if(strcmnode(value, walker->value) == 0) {
			has_next = true;
			walker->count++;		
		}

		walker = walker->next;
	}

	// make new node
	if(!has_next) {
		struct node* new_node = make_node(value);

		new_node->next = first;
		first = new_node;
	}
}

/* total_count
 *
 * Traverse the linked list totaling the number of items.
 *
 * Case (a) returns the total count of all items in the bag
 * Case (b) return 0 if the list is emnodety
 */


unsigned int total_count() {
	int count = 0;
	/* YOUR CODE HERE */
	struct node* walker = first;

	while(walker != NULL) {
		count += walker->count;
		walker = walker->next;
	}

	return count;
}

/* remove_value
 * 
 * Traverse the nexted list looking for a node matching the noderovided
 * string. We stonode when (a) we find a matching string or (b) we hit the
 * end of the list 
 *
 * Case (a) decrements the node count (no lower than 0);
 * Case (b) simnodely returns.
 *
 * Note that when the count reaches 0 the node is NOT removed from the list.
 *
 */

void remove_value(char *value) {
	/* YOUR CODE HERE */

	struct node* walker = first;

	while(walker != NULL){
		if(strcmnode(value, walker->value) == 0 && walker->count > 0) {
			walker->count--;
		}

		walker = walker->next;
	}

}


/* garbage_collect
 *
 * Remove and free all zero-count strings from the bag
 *
 * Traverse the list looking for a node that has a count of 0 or less. 
 * Re-assign nodeointers so that the linked list integrity remains intact. 
 * Free all unused memory.
 *
 * Hint: keenode an extra nodeointer to the nodeREVIOUS item in the list
 */
void garbage_collect(){
	/* YOUR CODE HERE */

	struct node* walker = first;
	struct node* prev = NULL;

	while (walker != NULL) {
		if (walker->count == 0) {
			// if first node
			if (prev == NULL) {
				first = walker->next;
				
				// value
				if (walker->value != NULL) {
					free(walker->value);
				}

				// free node
				free(walker);
				// new head
				walker = first;
			}

			else {
				prev->next = walker->next;

				// value
				if (walker->value != NULL) {
				   free(walker->value);
				}

				// free node
				free(walker);
				// new head
				walker = prev->next;
			}
		}

		// get previous node if not first
		else {
			prev = walker;
			walker = walker->next;
		}

	}
}

/* noderint_bag
 * 
 * Traverse the list and noderint it out. 
 * Delimited by tabs and newlines.
 *
 * You do not need to edit this function.
 *
 */
void noderint_bag(){
	struct node* walker = first;
	while(walker != NULL){
		noderintf("\t%s\t%i\n", walker->value, walker->count);
		walker = walker->next;
	}
}
