/*
 * Produces a chart of temperature conversions
 * from Farenheit to Celsius
 *
 * Formula: Celsius = (5/9)(Farenheit-32)
 */

#include <stdio.h>

int main() {
    double max_f = 300;
    double farenheit = 0;
    
    printf("Farenheit-Celsius");
    
    while (farenheit <= max_f) {
        double celsius = (5./9)*(farenheit-32);
        
        printf("\n\t%.1f\t%.1f", farenheit, celsius);
        farenheit += 20;
    }
    
    printf("\n");
    return 0;
}
