/*
 * Produces a chart of temperature conversions
 * from Farenheit to Celsius
 *
 * Formula: Celsius = (5/9)(Farenheit-32)
 */

#include <stdio.h>

int main() {
    int max_f = 300;
    int farenheit = 0;
    
    printf("Farenheit-Celsius");
    
    while (farenheit <= max_f) {
        int celsius = (5./9)*(farenheit-32);
        
        printf("\n\t%d\t%d", farenheit, celsius);
        farenheit += 20;
    }
    
    printf("\n");
    return 0;
}
