/*
 * Home Security System
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "hs_config.h"
#include "hs_util.h"
 
/*
 * Room event information
 * 	Total number of events that have been logged
 * 	Index of the newest event (if any)
 * 	Index of the oldest event (if any)
 * 	Buffer of (up to) MAX_EVENTS most recent events
 */
struct room_t {
	int event_count ;
	int newest ;
	int oldest ;
	struct event_t buffer[MAX_EVENTS] ;
} ;

/*
 * Rooms being monitored.
 */
#define MAX_ROOMS (5)
static struct room_t rooms[MAX_ROOMS] ;

/*
 * Local support functions (static).
 * Feel free to add more to make your work easier!
 */
static void process_a_reading(struct reading_t reading) ;
static void init_rooms() ; 
static void echo_reading( struct reading_t reading );

static int co_danger(int co_amount);
static int temp_danger(int temperature);
static void print_all_alerts(int room_id, struct event_t event);
static void buffer_add(int room_id, struct event_t event);
static void print_room(int sensor, char time_stamp[], int room_id);
static void print_event(struct event_t event);
static void print_alert(int room_id, struct event_t event);

	/*
 * Main driver function.
 *
 * First, initialize global data structures (rooms array).
 *
 * Next, read lines of text representing readings, parse these, and if there
 * are no syntax errors, record the associated events. Syntactically incorrect
 * input reading lines are silently discarded.
 */

	int main()
{
	char next_line[MAX_LINELENGTH+1] ;
	struct reading_t reading ;

	init_rooms() ;
	/*
	 * Read a line of text into next_line, then attempt to parse
	 * it as a reading line. If the line is parsable, process it according to the
	 * specification for the appropriate level.
	 * See hs_util.h for useful utility functions
	 */
	while( read_line(next_line, MAX_LINELENGTH) != EOF ) {
		if( parse_string(next_line, &reading) ) {
			process_a_reading(reading) ;   // uncomment for L2,L3,L4,L5 tests
			// echo_reading( reading );      // uncomment for L1 test only
		}
	}


	return 0 ;
}

/*
 * Given a reading, process the included event for the room in the reading.
 * T_PRINT readings are really commands; once executed they are discarded.
 * For all other readings check to see whether an alert should be printed,
 * then add the event as the newest event in the room's circular buffer.
 */
static void process_a_reading(struct reading_t reading)
{
	/* Your code here */
	struct event_t event = reading.event;
	int room_id = reading.room_id;


	if (event.event_id == T_PRINT) {
		print_room(event.sensor, event.time_stamp, room_id);
	}
	else {
		print_all_alerts(room_id, event);
		buffer_add(room_id, event);
	}
}

/*
 * Initialize the information on all rooms so that no rooms
 * have any events associated with them.
 */
static void init_rooms()
{
	int room_id ;

	/*
	 * Initialize rooms array so that all rooms have no
	 * events associated with them.
	 */

	for( room_id = 0 ; room_id < MAX_ROOMS ; room_id++ ) {
		rooms[room_id].event_count = 0 ;
		rooms[room_id].newest = 0 ;
		rooms[room_id].oldest = 0 ;
	}
}


//Add helper functions here

static void echo_reading(struct reading_t reading)
{
	printf("init variables\n");
	int room_id = reading.room_id;
	int sensor = reading.event.sensor;
	char time[MAX_TIMESTRING + 1];
	int event_id = reading.event.event_id;
	int event_info = reading.event.event_info;

	printf("echo reading variables\n");

	(void)strcpy(time, reading.event.time_stamp);

	printf("Room %d / Sensor %d: %s: ", room_id, sensor, time);
	switch (event_id)
	{
	case T_TEMPERATURE:
		printf("temperature reading %d degrees\n", event_info);
		break;
	case T_CO:
		printf("carbon monoxide reading %d PPB\n", event_info);
		break;
	case T_INTRUDER:
		printf("intruder alert\n");
		break;
	case T_PRINT:
		printf("print\n");
		break;
	}
}

/**
 * If co amount is too high
 * @return true(1) if dangerous, false(0) if not
 */
static int co_danger(int co_amount) {
	return co_amount > CO_LIMIT;
}

/**
 * If temp is too high or low
 * @return true(1) if too high or low, false(0) if not
 */
static int temp_danger(int temperature) {
	return temperature < MIN_TEMP || temperature > MAX_TEMP;
}

/**
 * Check if there are alerts for an event and if there is, print alert
 */
static void print_all_alerts(int room_id, struct event_t event) {
	if (event.event_id == T_CO) {
		if(co_danger(event.event_info))
		{
			printf("Carbon Monoxide");
			print_alert(room_id, event);
			printf(".\n");
		}
	}

	else if (event.event_id == T_TEMPERATURE) {
		if(temp_danger(event.event_info)) {
			printf("Temperature");
			print_alert(room_id, event);
			printf(" / %d degrees.\n", event.event_info);
		}
	}

	else if (event.event_id == T_INTRUDER) {
		printf("Intruder");
		print_alert(room_id, event);
		printf(".\n");
	}
}

/**
 * Add event to buffer
 * overwrites oldest entry if the buffer is full
 */
static void buffer_add(int room_id, struct event_t event) {
	struct room_t room = rooms[room_id];


	if (room.event_count == 0) {
		room.newest = room.oldest = 0;
	}

	else if (room.event_count < MAX_EVENTS) {
		room.newest++;
	}

	else {
		room.newest = (room.newest + 1) % MAX_EVENTS;
		room.oldest = (room.oldest + 1) % MAX_EVENTS;
	}

	room.buffer[room.newest] = event;
	room.event_count++;
	rooms[room_id] = room;
}

/**
 * print all information on the current room
 */
static void print_room(int sensor, char time_stamp[], int room_id) {
	struct room_t room = rooms[room_id];
	int current_event;

	// print vars = room_id, time_stamp, sensor, room.event_count
	printf("*****\nHome Security System: Room %d @ %s\nTriggered by sensor %d\n%d total room events\n", 
				room_id, time_stamp, sensor, room.event_count);

	current_event = room.newest;
	if (room.event_count != 0) {
		while (1) {
			print_event(room.buffer[current_event]);

			if (current_event == room.oldest) {
				break;
			}

			current_event += (MAX_EVENTS - 1);
			current_event %= MAX_EVENTS;
		}
	}
}

/**
 * Prints all event info
 * 
 * @param event event to be read and printed
 */
static void print_event(struct event_t event) {
	printf("Sensor %d @ %s ", event.sensor, event.time_stamp);

	if (event.event_id == T_CO) {
		printf("carbon monoxide reading %d PPB\n", event.event_info);
	}

	else if (event.event_id == T_TEMPERATURE) {
		printf("temperature reading %d degrees\n", event.event_info);
	}

	else if (event.event_id == T_INTRUDER) {
		printf("intruder alert\n");
	}
}

/**
 * Print alert
 * 
 * @param room_id room to print alert from
 * @param event	alert
 */
static void print_alert(int room_id, struct event_t event) {
	printf(" alert @ %s:", event.time_stamp);
	printf(" room %d / sensor %d", room_id, event.sensor);
}
