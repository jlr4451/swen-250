/*
 * Home Security System
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "hs_config.h"
#include "hs_util.h"

/*
 * An event node on the linked list of events for
 * a room. Consists of an event as gathered from a
 * reading and a link to the next node on the list
 * (if any).
 */
struct evnode_t {
	struct event_t event ;
	struct evnode_t *next ;
} ;

/*
 * Room event information
 * 	The room number.
 * 	Total number of events that have been logged.
 * 	Pointer to the most recent reading's node (if any).
 * 	Pointer to the next room (if any).
 */
struct room_t {
	int room ;
	int ecount ;
	struct evnode_t *event_list_head ;
	struct room_t *next_room ;
} ;

/*
 * Head of the list of rooms being monitored.
 */
static struct room_t *room_list = NULL ;

/*
 * Local support functions (static).
 * You have to fill in the details.
 * Feel free to add more to make your work easier!
 */
static void process_a_reading(struct reading_t reading) ;

static struct room_t *find_room(int room) ;
static struct room_t *add_new_room(int room) ;
static void trim_list(int room, int keep, char timestamp[]) ;

static struct evnode_t *make_event_node() ;

static int co_danger(int co_amount);
static int temp_danger(int temperature);
static void print_all_alerts(int room_id, struct event_t event);
static void event_add(int room_id, struct event_t event);
static void print_room(int sensor, char time_stamp[], int room_id);
static void print_event(struct event_t event);
static void print_alert(int room_id, struct event_t event);
static void print_trim_log(int room, int kept, int deleted, char timestamp[]);

/*
 * Main driver function.
 *
 * First, initialize global data structures (rooms array).
 *
 * Next, read lines of text representing readings, parse these, and if there
 * are no syntax errors, record the associated events. Syntactically incorrect
 * input reading lines are silently discarded.
 * 
 */
int main() {
	char next_line[MAX_LINELENGTH+1] ;
	struct reading_t reading ;

	/*
	 * Read a line of text into next_line, then attempt to parse
	 * it as a <reading> line. If the line is parsable, get the
	 * last reading structure and process it according to the
	 * specification for the appropriate level.
	 * See hs_util.h for useful utility functions.
	 * 
	 * At the end of the program, clean up any dynamically allocated memory
	 */
	
	while( read_line(next_line, MAX_LINELENGTH) != EOF ) {
		if( parse_string(next_line, &reading) ) {
			process_a_reading(reading) ;
			
		// NOTE - echo_reading() not required in Part 2
		}
	}


	return 0 ;
}

/*
 * Given a reading, process the included event for the room in the reading.
 * T_PRINT and T_TRIM readings are really commands; once executed they are
 * discarded.
 * For all other readings check to see whether an alert should be printed,
 * then add the event to as the newest event in the room's event list.
 */
static void process_a_reading(struct reading_t reading) {

	// FILL IN THE BODY
	// struct event_t event = reading.event;
	// int room_id = reading.room_id;


	// if (event.event_id == T_PRINT) {
	// 	print_room(event.sensor, event.time_stamp, room_id);
	// }
	// else if (event.event_id == T_TRIM) {
	// 	trim_list(room_id, event.sensor, event.time_stamp);
	// }
	// else {
	// 	print_all_alerts(room_id, event);
	// 	event_add(room_id, event);
	// }

	// return ;

	struct event_t event = reading.event ;

	int room_id = reading.room_id;

	if(event.event_id == T_PRINT) {
		print_room(event.sensor, event.time_stamp, room_id);
	}

	else if( event.event_id == T_TRIM ) {
		trim_list(room_id, event.event_info, event.time_stamp);
	}

	else {
		print_all_alerts(room_id, event);
		event_add(room_id, event);
	}
}

/**
 * If co amount is too high
 * @return true(1) if dangerous, false(0) if not
 */
static int co_danger(int co_amount) {
	return co_amount > CO_LIMIT;
}

/**
 * If temp is too high or low
 * @return true(1) if too high or low, false(0) if not
 */
static int temp_danger(int temperature) {
	return temperature < MIN_TEMP || temperature > MAX_TEMP;
}

/**
 * Check if there are alerts for an event and if there is, print alert
 */
static void print_all_alerts(int room_id, struct event_t event) {
	int event_id = event.event_id;

	if (event_id == T_CO) {
		if(co_danger(event.event_info))
		{
			printf("Carbon monoxide");
			print_alert(room_id, event);
			printf(" / %d PPB.\n", event.event_info) ;
		}
	}

	else if (event_id == T_TEMPERATURE) {
		if(temp_danger(event.event_info)) {
			printf("Temperature");
			print_alert(room_id, event);
			printf(" / %d degrees.\n", event.event_info);
		}
	}

	else if (event_id == T_INTRUDER) {
		printf("Intruder");
		print_alert(room_id, event);
		printf(".\n");
	}
}

/**
 * Add event to list
 * If first time calling room, add event to head of room events
 */
static void event_add(int room_id, struct event_t event) {
	struct room_t *the_room = find_room(room_id) ;

	if(!the_room) {
		the_room = add_new_room(room_id);
	}

	the_room->ecount++;

	struct evnode_t *new_node = make_event_node(event);

	new_node->next = the_room->event_list_head;
	the_room->event_list_head = new_node;
}

/**
 * print all information on the current room
 */
static void print_room(int sensor, char time_stamp[], int room_id) {
	struct room_t *room = find_room(room_id);

	// print vars = room_id, time_stamp, sensor
	printf("*****\nHome Security System: Room %d @ %s\nTriggered by sensor %d\n", 
				room_id, time_stamp, sensor);
	if (room)
		printf("%d total room events\n", room->ecount);
	else
		printf("0 total room events\n");


	struct evnode_t *new_node;
	if (room)
		new_node = room->event_list_head;
	else
		new_node = NULL;

	while (new_node) {
		print_event(new_node->event);
		new_node = new_node->next;
	}
}

/**
 * Prints all event info
 * 
 * @param event event to be read and printed
 */
static void print_event(struct event_t event) {
	printf("Sensor %d @ %s ", event.sensor, event.time_stamp);

	int event_id = event.event_id;

	if (event_id == T_CO) {
		printf("carbon monoxide reading %d PPB\n", event.event_info);
	}

	else if (event_id == T_TEMPERATURE) {
		printf("temperature reading %d degrees\n", event.event_info);
	}

	else if (event_id == T_INTRUDER) {
		printf("intruder alert\n");
	}
}

/**
 * Print alert
 * 
 * @param room_id room to print alert from
 * @param event	alert
 */
static void print_alert(int room_id, struct event_t event) {
	printf(" alert @ %s:", event.time_stamp);
	printf(" room %d / sensor %d", room_id, event.sensor);
}

/*
 * Find the specified <room> in the <room_list>, returning a pointer to the
 * found room_t structure or NULL if there is no such <room> in the list.
 */
static struct room_t *find_room(int room)
{
	struct room_t *the_room ;

	// FILL IN THE BODY
	the_room = room_list;
	while (the_room && the_room->room != room) {
		the_room = the_room->next_room;
	}

	return the_room ;
}

/*
 * Create a new room_t node for <room>, initialize its fields, and append
 * the node to the end of the <room_list>.
 * Returns a pointer to the new room's structure.
 */
static struct room_t *add_new_room(int room) 
{
	struct room_t *new_room ; // ptr. to new room structure

	// FILL IN THE BODY
	new_room = malloc(sizeof(struct room_t));
	new_room->room = room;
	new_room->ecount = 0;
	new_room->event_list_head = NULL;
	new_room->next_room = NULL;

	struct room_t *next_room = room_list;
	struct room_t *prev_room = NULL;

	/*
	 * Find the end of the list and append the new room.
	 */
	while (next_room) {
		prev_room = next_room;
		next_room = next_room->next_room;
	}

	if (prev_room) {
		prev_room->next_room = new_room;
	}
	else {
		room_list = new_room;
	}

	return new_room ;
}

/*
 * If the <room> is in the <room_list>, trim the room's event node list
 * to at most <keep> entries. As the list is ordered from newest to
 * oldest, the oldest entries are discarded.
 *
 * Whether the room exists or not, and whether or not the list holds
 * anything, the specified "Trim log" message must be printed. Obviously,
 * for a non-existent room nothing is actually trimmed (removed).
 *
 * Note - dynamically allocated space for event nodes removed from
 *        the list must be freed.
 */
static void trim_list(int room, int keep, char timestamp[]) {
	// FILL IN THE BODY

	struct room_t *the_room;
	
	struct evnode_t *current;
	struct evnode_t *previous;	

	int kept = keep;
	int deleted = 0;

	if (!(the_room = find_room(room))) {
		print_trim_log(room, keep, deleted, timestamp);
	}
	else {
		previous = NULL ;
		current = the_room->event_list_head;

		while (kept-- > 0 && current) {
			previous = current;
			current = current->next;
		}
	}

	if (previous)
		previous->next = NULL;
	else
		the_room->event_list_head = NULL;


	while(current) {
		deleted++;

		previous = current;
		current = current->next;

		free(previous) ;
	}

	print_trim_log(room, keep, deleted, timestamp) ;

	return ;
}

/**
 * print trim log
 */
static void print_trim_log(int room, int kept, int deleted, char timestamp[]) {
	// print vars = timestamp, room, kept, deleted
	printf("Trim log @ %s: room %d log trimmed to length %d (%d entries removed)\n", timestamp, room, kept, deleted);
}

/*
 * Create a new evnode_t node, initialize it using the provided
 * event and a NULL link, and return the pointer to the node.
 */
static struct evnode_t *make_event_node(struct event_t event) {
	struct evnode_t *new_evnode ;

	// FILL IN THE BODY
	new_evnode = malloc(sizeof(struct evnode_t)) ;
	new_evnode->event = event;
	new_evnode->next = NULL;


	return new_evnode ;
}

