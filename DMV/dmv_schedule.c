/***
 * Functions for the DMV customer scheduling and service application.
 * Implementation file.
 ***/

#include <stdlib.h>
#include "dmv_schedule.h"

/*
 * Actual definition of the line array of service lines.
 */
struct service_line line[NLINES] ;

/*
 * Initialize the service lines so that (a) line[0] has priority 'A'
 * through line[2] having priority 'C' and (b) there are no customers
 * in any line.
 * NOTES: As usual, an empty list is signified by a NULL pointer.
 */
void init_service_lines() {
	// Placeholder for your code
	for (int i = 0; i < NLINES; i++) {
		line[i].priority = 'A' + i;
		line[i].head_of_line = NULL;
	}
}

/*
 * Return the next ticket number.
 * 	The first customer gets ticket number 1.
 *	The number increases by 1 on each call.
 * Local (static) int ticket holds the current ticket value.
 */
static int ticket = 1 ;
int next_ticket() {
	ticket += 1;
	return ticket - 1;
}

/*
 * A new customer arrives with the given <priority> and
 * <ticket_number>. The customer is placed at the end of
 * the appropriate service line.
 */
void new_customer(char priority, int ticket_number) {
	// Placeholder for your code. 
	int priority_line = priority - 'A';
	struct customer *current_customer = line[priority_line].head_of_line;

	struct customer *p_customer_new = calloc(1, sizeof(struct customer));
	p_customer_new->ticket_number = ticket_number;
	p_customer_new->next_customer = NULL;

	if (!current_customer) {
		line[priority_line].head_of_line = p_customer_new;
	}
	else {
		while(current_customer->next_customer) {
			current_customer = current_customer->next_customer;
		}

		current_customer->next_customer = p_customer_new;
	}
}

/*
 * Return the ticket number of the first customer in the
 * line for <priority> after removing the customer from the
 * associated service_line.
 *
 * Return NO_CUSTOMER if there are no customers in the indicated line.
 */
int serve_customer(char priority) {
	int priority_line = priority - 'A';
	struct customer *current_customer = line[priority_line].head_of_line;

	if (!current_customer) {
		return NO_CUSTOMER;
	}
	else {
		line[priority_line].head_of_line = current_customer->next_customer;

		int ticket_number = current_customer->ticket_number;

		free(current_customer->ticket_number);
		free(current_customer);

		return ticket_number;
	}
}

/*
 * Return the ticket number of the first customer in the highest
 * priority line that has customers after removing the customer
 * from the line. 'A' is highest priority and 'C' is lowest.
 *
 * Return NO_CUSTOMER if there are no customers in any of the lines.
 *
 * Example: if there are 0 customers in the 'A' line, 3 customers in the 'B'
 *          line and 2 customers in the 'C' line, then the first customer in
 *          the 'B' line would be removed and his/her ticket number returned.
 */
int serve_highest_priority_customer() {
	for (char c = 'A'; c <= 'C'; c++) {
		if (line[c - 'A'].head_of_line) {
			return serve_customer(c);
		}
	}
	return NO_CUSTOMER;
}

/*
 * Return the number of customers in the service line for <priority>
 */
int customer_count(char priority) {
	int count = 0;

	struct customer *current_customer = line[priority - 'A'].head_of_line;

	if (!current_customer) {
		return count;
	}
	else {
		count++;

		while (current_customer->next_customer) {
			current_customer = current_customer->next_customer;
			count++;
		}

		return count;
	}

}

/*
 * Return the number of customers in all service lines.
 */
int customer_count_all() {
	int count = 0;

	for (char c = 'A'; c <= 'C'; c++) {
		count += customer_count(c);
	}

	return count; // Placeholder for your code.
}
