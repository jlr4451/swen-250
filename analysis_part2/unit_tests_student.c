// unit_tests student additions
// Larry L. Kiser Oct. 22, 2015

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>

#include "analysis.h"
#include "unit_tests_analysis.h"
#include "unit_tests.h"

//////////////////////////////////////////////////////////////////////////
// Begin unit tests //////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// do the unit tests
// returns the number of failed tests.
int test_student()
{
	int passcount = 0 ;
	int failcount = 0 ;
	struct linked_list list1 ;		// list list pointers for one list
	
	// Initialize local stack data
	memset( &list1, 0, sizeof( list1 ) ) ;		// set all pointers to NULL (zero)
	
	printf( "\n========= student generated tests on MLK excerpt ===============================\n" ) ;
	int result = read_file( &list1, "MLK_dream_excerpt.txt" ) ;
	assert( result == 113,
		"1 read_file processing MLK_dream_excerpt. Expect word count of 113 returned but got %d.", result )
		? passcount++ : failcount++ ;

	// Add a test to verify that the word "day" is used three times in the list.
	printf( "\n========= test verifying day is used three times in the list ===================\n" ) ;
	find_word(&list1, "day");
	result = list1.p_current->one_word.word_count;
	assert( result == 3,
		"1 read_file processing MLK_dream_excerpt. Expect word count of 3 returned but got %d.", result )
		? passcount++ : failcount++ ;
	
	// Add a test to verify that the word AFTER the word "have" is the word "heat".
	printf( "\n========= test verifying that AFTER the word have is heat ======================\n" ) ;
	find_word(&list1, "have");
	char* char_result = list1.p_current->p_next->one_word.unique_word;
	result = strcmp(char_result, "heat");
	assert( result == 0,
		"1 read_file processing MLK_dream_excerpt. Expected word heat to be after have but was %c.", char_result)
		? passcount++ : failcount++ ;

	// Add a test to verify that the last word in the list is "with"
	printf( "\n========= test verifying that the last word is with ============================\n" ) ;
	char_result = list1.p_tail->one_word.unique_word;
	result = strcmp(char_result, "with");
	assert( result == 0,
		"1 read_file processing MLK_dream_excerpt. Expect word with to be last but was %c.", char_result )
		? passcount++ : failcount++ ;
		
	// Add a test to verify that the next to the last word is "will"	
	printf( "\n========= test verifying the next to last word is will =========================\n" ) ;
	char_result = list1.p_tail->p_previous->one_word.unique_word;
	result = strcmp(char_result, "will");
	assert( result == 0,
		"1 read_file processing MLK_dream_excerpt. Expect word will to be next to last but was %c.", char_result )
		? passcount++ : failcount++ ;	

	result = clear_linked_list( &list1 ) ;
	assert( result == 62,
		"6 clear linked list expected to remove 62 entries but got %d.", result )
		? passcount++ : failcount++ ;

	printf( "\nSummary of analysis unit tests:\n%d tests passed\n%d tests failed\n\n", passcount, failcount ) ;
	
	return failcount ;
}